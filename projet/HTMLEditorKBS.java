package projet;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.html.HTMLAnchorElement;

import javafx.application.Application;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class HTMLEditorKBS extends Application {
	
	//TODO : Remove the link (new button)
	
	private Map<String,String> urls = new HashMap< String,String>();
	private int countUrl;
	private WebView webView;
	private String selected;
	private WebEngine engine;
	private Hyperlink link;
	private static final int MIN_LETTRES_SELECTION =3;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) {
		countUrl=0;
		stage.setTitle("HTMLEditor");
		stage.setWidth(660);
		stage.setHeight(500);
		final HTMLEditor editor = new HTMLEditor();

		//Bar
		Node toolNode = editor.lookup(".top-toolbar");

		//WebView
		Node webNode = editor.lookup(".web-view");
		webView = (WebView) editor.lookup("WebView");
		engine = webView.getEngine();

		//SETHTML
		editor.setHtmlText("Hello World,<br><br> This is my hypertext link to <a href=\"http://www.google.com\" title=\"google\" id=\"URL0\" target=\"_blank\">google</a>.<br><br> Thanks a million !");
		
		engine.getLoadWorker().stateProperty().addListener((observable, oldState, newState) -> {
		    if (newState == State.SUCCEEDED) {
		    	Document document = engine.getDocument();
		        updateDom(document);
		    }
		});
		
		
        
		if (toolNode instanceof ToolBar && webNode instanceof WebView) {
			ToolBar bar = (ToolBar) toolNode;
			createButton(editor, bar);
			editor.setPrefHeight(445);

			VBox root = new VBox();     
			root.setPadding(new Insets(8, 8, 8, 8));
			root.setSpacing(5);
			root.setAlignment(Pos.TOP_LEFT);

			Scene scene = new Scene(new Group());  

			root.getChildren().addAll(editor);
			scene.setRoot(root);

			stage.setScene(scene);
			stage.isResizable();
			stage.show();
		}
	}
	private EventListener[] eventListener;
	private void updateDom(Document document) {
		NodeList nodeList = document.getElementsByTagName("a");
		eventListener = new EventListener[nodeList.getLength()];
        for (int i = 0; i < nodeList.getLength(); i++)
        {
        	org.w3c.dom.Node node= nodeList.item(i);
            EventTarget eventTarget = (EventTarget) node;
            eventListener[i]=new EventListener()
            {
                @Override
                public void handleEvent(Event evt)
                {
                    EventTarget target = evt.getCurrentTarget();
                    HTMLAnchorElement anchorElement = (HTMLAnchorElement) target;
                    String href = anchorElement.getHref();
                    overrideHyperTextJava(href);
                    evt.preventDefault();
                }
            };
            eventTarget.addEventListener("click", eventListener[i], false);
        }
	}

	private void createButton(final HTMLEditor editor, ToolBar bar) {
		Button btnHL = new Button("Hyperlink");
		Button btnHLBk = new Button("HyperlinkBroken");
		// TODO : SWITCH IMAGES
		// Internet/local
		// > Image retrieved from internet
//				final String imageURI = "https://www.techjini.com/wp-content/uploads/2006/01/link-icon-png-14.png"; 
//				ImageView iv = new javafx.scene.image.ImageView(new Image(imageURI));
//		 		final String imageURI1 = "https://ya-webdesign.com/transparent450_/svg-link-broken.png";
//				ImageView iv1 = new javafx.scene.image.ImageView(new Image(imageURI1));
		// > Image retrieved from local
		ImageView iv = new javafx.scene.image.ImageView(new Image(getClass().getResourceAsStream("/projet/hyperlink.png")));
		ImageView iv1 = new javafx.scene.image.ImageView(new Image(getClass().getResourceAsStream("/projet/hyperlink-broken.png")));

		btnHL.setMinSize(26.0, 22.0);
		btnHL.setMaxSize(26.0, 22.0);
		btnHLBk.setMinSize(26.0, 22.0);
		btnHLBk.setMaxSize(26.0, 22.0);
		iv.setFitHeight(20);
		iv.setFitWidth(20);
		iv1.setFitHeight(18);
		iv1.setFitWidth(18);
		btnHL.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		btnHL.setGraphic(iv);
		btnHL.setTooltip(new javafx.scene.control.Tooltip("Hyperlink"));
		btnHLBk.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		btnHLBk.setGraphic(iv1);
		btnHLBk.setTooltip(new javafx.scene.control.Tooltip("HyperlinkBroken"));
		btnHL.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				addHyperLinksJS();
				OverrideHyperTextJS();
			}
		});
		btnHLBk.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				removeHyperLinksJS();
			}
		});
		
		bar.getItems().addAll(btnHL
				,btnHLBk
				);

	}
	private String dialogCollectUrl(String selected) {
		String url=null;
		TextInputDialog dialog = new TextInputDialog("http://www.google.com");
		dialog.setTitle("Set the hyperlink");
		dialog.setHeaderText("Enter the URL you want for this selection \""+selected+"\"");
		dialog.setHeight(100);dialog.setWidth(100);//dialog.setX(100);dialog.setY(100);
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()&& dialog.getResultConverter().call(ButtonType.OK)!=null){
			url= result.get();
			urls.put(selected, url);
			countUrl++;
		}else if (dialog.getResultConverter().call(ButtonType.CANCEL)!=null||dialog.getResultConverter().call(ButtonType.CANCEL)==null) {

		}
		return url;
	}
	public static boolean IsMatch(String s) {
		try {
			final Pattern patt = Patterns.WEB_URL;
			Matcher matcher = patt.matcher(s);
			return matcher.matches();
		} catch (RuntimeException e) {
			return false;
		}
	}

	public String overrideHyperTextJava(String url) {
		link = new Hyperlink(this.selected);
		link.setTooltip(new Tooltip(url));
		System.out.println("Debut overrideHyperTextJava");
		if(url=="x"||url=="http://www.x"||url==null||!IsMatch(url)){
			System.out.println("URL non correct");
			return null;//Ne rien faire
		}else {
			System.out.println("URL correct");
			//			link.setOnAction((ActionEvent event) -> {
			//				Hyperlink h = (Hyperlink) event.getTarget();
			//				String s = h.getTooltip().getText();
			//				getHostServices.showDocument(s);
			//				event.consume();
			//			});
			link.setOnAction((ActionEvent event) -> {
//				URI uri = null;
//				System.out.println("Change url to URI");
//				try {
//					uri = new URI(url);
//				} catch (URISyntaxException e1) {
//					e1.printStackTrace();
//					System.out.println("e1.printStackTrace()");
//				}
//				final Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
//				if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
//					try {
//						desktop.browse(uri);
//					} catch (IOException e) {
//						e.printStackTrace();
//						System.out.println("e.printStackTrace()");
//					}
//				} else {
//
//					throw new UnsupportedOperationException("Browse action not supported");
//				}
				getHostServices().showDocument(url);
				
			});
			link.fire();
			return null;
		}
	}
	//TODO : Remove Hyperlinks
	boolean isSelected;
	private void removeHyperLinksJS(){
		System.out.println("Debut removeHyperLinksJS");
		selected = (String) webView.getEngine().executeScript(SELECT_TEXT);
		Document document = engine.getDocument();
		org.w3c.dom.NodeList nodeList= document.getElementsByTagName("a");
		System.out.println(nodeList.toString());
        for (int i = 0; i < nodeList.getLength(); i++)
        {
        	org.w3c.dom.Node node= nodeList.item(i);
            EventTarget eventTarget = (EventTarget) node;
            //TODO
            if(isSelected) {
				eventTarget.removeEventListener("click",eventListener[i],false);
				System.out.println(node.toString());
				node.removeChild(node);
			}
        }
	}
	
	
	private void addHyperLinksJS(){
		System.out.println("Debut addHyperLinksJS");
		selected = (String) webView.getEngine().executeScript(SELECT_TEXT);
		if(selected != ""&&selected != null) {
			if(selected.length() > MIN_LETTRES_SELECTION) {
				String url2 = dialogCollectUrl(selected);
				if(IsMatch(url2)) {
					//				webView.getEngine().executeScript(restoreSelection(selected));
					//Make the link :
					//				webView.getEngine().executeScript("document.execCommand(\"CreateLink\", false, "+url2+")");
					//				String urls2 = (String) webView.getEngine().executeScript(getLinksInSelection());
					//				String[] url2Table= (urls2.split(",")) ;
					String hyperlinkHtml = "<a href=\"" + url2.trim() + 
							"\" title=\"" + selected +
							"\" id=\"URL" + countUrl + 
							"\" target=\"_blank\""+ 
							">" + selected + "</a>";
					System.out.println(hyperlinkHtml);
					webView.getEngine().executeScript(getInsertHtmlAtCursorJS(hyperlinkHtml));
					Document document = engine.getDocument();
			        updateDom(document);
					
				}else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Information Dialog");
					alert.setHeaderText(null);
					alert.setContentText("Hypertext not valid or operation cancelled!");

					alert.showAndWait();
				}
			}else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Information Dialog");
				alert.setHeaderText(null);
				alert.setContentText("The word for the selection is too short");
				alert.showAndWait();
			}
		}else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("There is no selection");
			alert.showAndWait();
		}
	}
	// JAVASCRIPT
	private String OverrideHyperTextJS(){
		return "document.onclick = "+
				"(function (e) {" +
				" 	  e = e ||  window.event;" +
				" 	  var element = e.target || e.srcElement;" +
				" 	  if (element.tagName == 'A') {" +
				"		var x = element.href;" +
				"		var y= " +overrideHyperTextJava("x")+";" +
				" 	    return false; " +// prevent default action and stop event propagation
				" 	  }" +
				" 	});"
				;
	}
	private String getInsertHtmlAtCursorJS(String html){
		return "insertHtmlAtCursor('" + html + "');"
				+ "function insertHtmlAtCursor(html) {\n"
				+ " var range, node;\n"
				+ " if (window.getSelection && window.getSelection().getRangeAt) {\n"
				+ " window.getSelection().deleteFromDocument();\n"
				+ " range = window.getSelection().getRangeAt(0);\n"
				+ " node = range.createContextualFragment(html);\n"
				+ " range.insertNode(node);\n"
				+ " } else if (document.selection && document.selection.createRange) {\n"
				+ " document.selection.createRange().pasteHTML(html);\n"
				+ " document.selection.clear();"
				+ " }\n"
				+ "}";
	}

	private String SELECT_TEXT =
			"(function getSelectionText() {\n" +
					"    var text = \"\";\n" +
					"    if (window.getSelection) {\n" +
					"        text = window.getSelection().toString();\n" +
					"    } else if (document.selection && document.selection.type != \"Control\") {\n" +
					"        text = document.selection.createRange().text;\n" +
					"    }\n" +
					// 				// Vider selection :
					//            "    if (window.getSelection) {\n" +
					//            "      if (window.getSelection().empty) {  // Chrome\n" +
					//            "        window.getSelection().empty();\n" +
					//            "      } else if (window.getSelection().removeAllRanges) {  // Firefox\n" +
					//            "        window.getSelection().removeAllRanges();\n" +
					//            "      }\n" +
					//            "    } else if (document.selection) {  // IE?\n" +
					//            "      document.selection.empty();\n" +
					//            "    }" +
					"    return text;\n" +
					"})()";

//	private String addFormat() { 
//		return "(function addFormat() {\n" +
//				" var savedSel = saveSelection();\r\n" + 
//				"    if(savedSel != '') {\r\n" + 
//				"        var url1 =prompt(\"Please enter the link for this selection \\n \"+savedSel+\"\",\"\");\r\n" + 
//				"        restoreSelection(savedSel);\r\n" + 
//				"        document.execCommand(\"CreateLink\", false, url1);\r\n" + 
//				"        var urls1 = getLinksInSelection();\r\n" + 
//				"        for (var i = 0; i < urls1.length; ++i) {\r\n" + 
//				"            urls1[i].setAttribute('target','_blank');\r\n" + 
//				"        }\r\n" + 
//				"    } else { alert(\"Please select some text to insert the link\"); }"
//				+ "})"
//				+ " function getLinksInSelection() {\r\n" + 
//				"        var selectedLinks = [];\r\n" + 
//				"        var range, containerEl, links, linkRange;\r\n" + 
//				"        if (window.getSelection) {\r\n" + 
//				"            sel = window.getSelection();\r\n" + 
//				"            if (sel.getRangeAt && sel.rangeCount) {\r\n" + 
//				"                linkRange = document.createRange();\r\n" + 
//				"                for (var r = 0; r < sel.rangeCount; ++r) {\r\n" + 
//				"                    range = sel.getRangeAt(r);\r\n" + 
//				"                    containerEl = range.commonAncestorContainer;\r\n" + 
//				"                    if (containerEl.nodeType != 1) {\r\n" + 
//				"                        containerEl = containerEl.parentNode;\r\n" + 
//				"                    }\r\n" + 
//				"                    if (containerEl.nodeName.toLowerCase() == \"a\") {\r\n" + 
//				"                        selectedLinks.push(containerEl);\r\n" + 
//				"                    } else {\r\n" + 
//				"                        links = containerEl.getElementsByTagName(\"a\");\r\n" + 
//				"                        for (var i = 0; i < links.length; ++i) {\r\n" + 
//				"                            linkRange.selectNodeContents(links[i]);\r\n" + 
//				"                            if (linkRange.compareBoundaryPoints(range.END_TO_START, range) < 1 && linkRange.compareBoundaryPoints(range.START_TO_END, range) > -1) {\r\n" + 
//				"                                selectedLinks.push(links[i]);\r\n" + 
//				"                            }\r\n" + 
//				"                        }\r\n" + 
//				"                    }\r\n" + 
//				"                }\r\n" + 
//				"                linkRange.detach();\r\n" + 
//				"            }\r\n" + 
//				"        } else if (document.selection && document.selection.type != \"Control\") {\r\n" + 
//				"            range = document.selection.createRange();\r\n" + 
//				"            containerEl = range.parentElement();\r\n" + 
//				"            if (containerEl.nodeName.toLowerCase() == \"a\") {\r\n" + 
//				"                selectedLinks.push(containerEl);\r\n" + 
//				"            } else {\r\n" + 
//				"                links = containerEl.getElementsByTagName(\"a\");\r\n" + 
//				"                linkRange = document.body.createTextRange();\r\n" + 
//				"                for (var i = 0; i < links.length; ++i) {\r\n" + 
//				"                    linkRange.moveToElementText(links[i]);\r\n" + 
//				"                    if (linkRange.compareEndPoints(\"StartToEnd\", range) > -1 && linkRange.compareEndPoints(\"EndToStart\", range) < 1) {\r\n" + 
//				"                        selectedLinks.push(links[i]);\r\n" + 
//				"                    } \r\n" + 
//				"                }\r\n" + 
//				"            }\r\n" + 
//				"        }\r\n" + 
//				"        return selectedLinks;\r\n" + 
//				"    }\r\n" + 
//				"function saveSelection() {\r\n" + 
//				"    if (window.getSelection) {\r\n" + 
//				"        sel = window.getSelection();\r\n" + 
//				"        if (sel.getRangeAt && sel.rangeCount) {\r\n" + 
//				"            var ranges = [];\r\n" + 
//				"            for (var i = 0, len = sel.rangeCount; i < len; ++i) {\r\n" + 
//				"                ranges.push(sel.getRangeAt(i));\r\n" + 
//				"            }\r\n" + 
//				"            return ranges;\r\n" + 
//				"        }\r\n" + 
//				"    } else if (document.selection && document.selection.createRange) {\r\n" + 
//				"        return document.selection.createRange();\r\n" + 
//				"    }\r\n" + 
//				"    return null;\r\n" + 
//				"}\r\n" + 
//				"function restoreSelection(savedSel) {\r\n" + 
//				"    if (savedSel) {\r\n" + 
//				"        if (window.getSelection) {\r\n" + 
//				"            sel = window.getSelection();\r\n" + 
//				"            sel.removeAllRanges();\r\n" + 
//				"            for (var i = 0, len = savedSel.length; i < len; ++i) {\r\n" + 
//				"                sel.addRange(savedSel[i]);\r\n" + 
//				"            }\r\n" + 
//				"        } else if (document.selection && savedSel.select) {\r\n" + 
//				"            savedSel.select();\r\n" + 
//				"        }\r\n" + 
//				"    }\r\n" + 
//				"}"
//				; 
//	}
	//	private String getLinksInSelection(){
	//		return "(function getLinksInSelection()"
	//				+ "{"+
	//				//			";"+
	//				//			"(" +
	//				//					" function getLinksInSelection() {\r\n" + 
	//				"        var selectedLinks = [];\r\n" + 
	//				"        var range, containerEl, links, linkRange;\r\n" + 
	//				"        if (window.getSelection) {\r\n" + 
	//				"            sel = window.getSelection();\r\n" + 
	//				"            if (sel.getRangeAt && sel.rangeCount) {\r\n" + 
	//				"                linkRange = document.createRange();\r\n" + 
	//				"                for (var r = 0; r < sel.rangeCount; ++r) {\r\n" + 
	//				"                    range = sel.getRangeAt(r);\r\n" + 
	//				"                    containerEl = range.commonAncestorContainer;\r\n" + 
	//				"                    if (containerEl.nodeType != 1) {\r\n" + 
	//				"                        containerEl = containerEl.parentNode;\r\n" + 
	//				"                    }\r\n" + 
	//				"                    if (containerEl.nodeName.toLowerCase() == \"a\") {\r\n" + 
	//				"                        selectedLinks.push(containerEl);\r\n" + 
	//				"                    } else {\r\n" + 
	//				"                        links = containerEl.getElementsByTagName(\"a\");\r\n" + 
	//				"                        for (var i = 0; i < links.length; ++i) {\r\n" + 
	//				"                            linkRange.selectNodeContents(links[i]);\r\n" + 
	//				"                            if (linkRange.compareBoundaryPoints(range.END_TO_START, range) < 1 && linkRange.compareBoundaryPoints(range.START_TO_END, range) > -1) {\r\n" + 
	//				"                                selectedLinks.push(links[i]);\r\n" + 
	//				"                            }\r\n" + 
	//				"                        }\r\n" + 
	//				"                    }\r\n" + 
	//				"                }\r\n" + 
	//				"                linkRange.detach();\r\n" + 
	//				"            }\r\n" + 
	//				"        } else if (document.selection && document.selection.type != \"Control\") {\r\n" + 
	//				"            range = document.selection.createRange();\r\n" + 
	//				"            containerEl = range.parentElement();\r\n" + 
	//				"            if (containerEl.nodeName.toLowerCase() == \"a\") {\r\n" + 
	//				"                selectedLinks.push(containerEl);\r\n" + 
	//				"            } else {\r\n" + 
	//				"                links = containerEl.getElementsByTagName(\"a\");\r\n" + 
	//				"                linkRange = document.body.createTextRange();\r\n" + 
	//				"                for (var i = 0; i < links.length; ++i) {\r\n" + 
	//				"                    linkRange.moveToElementText(links[i]);\r\n" + 
	//				"                    if (linkRange.compareEndPoints(\"StartToEnd\", range) > -1 && linkRange.compareEndPoints(\"EndToStart\", range) < 1) {\r\n" + 
	//				"                        selectedLinks.push(links[i]);\r\n" + 
	//				"                    } \r\n" + 
	//				"                }\r\n" + 
	//				"            }\r\n" + 
	//				"        }\r\n" + 
	//				"        return selectedLinks;\r\n" + 
	//				"    }\r\n"
	//				; 
	//	}
	//	private String saveSelection(){
	//		return "(function saveSelection()"
	//				//				+ ";"+
	//				//			"(" +
	//				//					"function saveSelection() "
	//				+ "{\r\n" + 
	//				"    if (window.getSelection) {\r\n" + 
	//				"        sel = window.getSelection();\r\n" + 
	//				"        if (sel.getRangeAt && sel.rangeCount) {\r\n" + 
	//				"            var ranges = [];\r\n" + 
	//				"            for (var i = 0, len = sel.rangeCount; i < len; ++i) {\r\n" + 
	//				"                ranges.push(sel.getRangeAt(i));\r\n" + 
	//				"            }\r\n" + 
	//				"            return ranges;\r\n" + 
	//				"        }\r\n" + 
	//				"    } else if (document.selection && document.selection.createRange) {\r\n" + 
	//				"        return document.selection.createRange();\r\n" + 
	//				"    }\r\n" + 
	//				"    return null;\r\n" + 
	//				"})\r\n";
	//	}
	//	private String restoreSelection(String savedSel){
	//		return "(function restoreSelection(savedSel)"
	//				//				+ ";"+
	//				//			"(" + 
	//				//					"function restoreSelection(savedSel) "
	//				+ "{\r\n" + 
	//				"    if (savedSel) {\r\n" + 
	//				"        if (window.getSelection) {\r\n" + 
	//				"            sel = window.getSelection();\r\n" + 
	//				"            sel.removeAllRanges();\r\n" + 
	//				"            for (var i = 0, len = savedSel.length; i < len; ++i) {\r\n" + 
	//				"                sel.addRange(savedSel[i]);\r\n" + 
	//				"            }\r\n" + 
	//				"        } else if (document.selection && savedSel.select) {\r\n" + 
	//				"            savedSel.select();\r\n" + 
	//				"        }\r\n" + 
	//				"    }\r\n" + 
	//				"})";
	//	}
//    public static final String SELECT_HTML
//    = "(getSelectionHTML = function () {\n"
//    + "      var userSelection;\n"
//    + "      if (window.getSelection) {\n"
//    + "        // W3C Ranges\n"
//    + "        userSelection = window.getSelection ();\n"
//    + "        // Get the range:\n"
//    + "        if (userSelection.getRangeAt)\n"
//    + "          var range = userSelection.getRangeAt (0);\n"
//    + "        else {\n"
//    + "          var range = document.createRange ();\n"
//    + "          range.setStart (userSelection.anchorNode, userSelection.anchorOffset);\n"
//    + "          range.setEnd (userSelection.focusNode, userSelection.focusOffset);\n"
//    + "        }\n"
//    + "        // And the HTML:\n"
//    + "        var clonedSelection = range.cloneContents ();\n"
//    + "        var div = document.createElement ('div');\n"
//    + "        div.appendChild (clonedSelection);\n"
//    + "        return div.innerHTML;\n"
//    + "      } else if (document.selection) {\n"
//    + "        // Explorer selection, return the HTML\n"
//    + "        userSelection = document.selection.createRange ();\n"
//    + "        return userSelection.htmlText;\n"
//    + "      } else {\n"
//    + "        return '';\n"
//    + "      }\n"
//    + "    })()";
}

